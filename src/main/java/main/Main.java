package main;

import config.Config;
import dao.SetupDao;
import model.Money;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import service.TransferService;

public class Main {

    public static void main(String[] args) throws Exception {

        //skeem luuakse alate ennem kui springi konteks luuakse
        new SetupDao().createSchema();

        ApplicationContext ctx =
              new AnnotationConfigApplicationContext(Config.class);


        TransferService ts = ctx.getBean(TransferService.class);

        ts.transfer(new Money(1, "EUR"), "Konto A", "Konto B");


        ((ConfigurableApplicationContext) ctx).close();
    }
}